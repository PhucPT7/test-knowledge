﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace TestKnowledge
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Ask the user to choose an option.
            Console.WriteLine("Choose an option from the following list:");
            Console.WriteLine("\ta - Multiphy String.");
            Console.WriteLine("\tb - Swap 2 number use tmp variable.");
            Console.WriteLine("\tc - Nomalize string.");
            Console.WriteLine("\td - Count even and odd numbers.");
            Console.WriteLine("\te - Calculator.");

            Console.Write("Your option? ");

            // Use a switch statement to do the math.
            switch (Console.ReadLine())
            {
                case "a":
                    Console.WriteLine("Input string: ");
                    string inputString = Console.ReadLine();
                    Console.WriteLine("Input times: ");
                    int times = Int32.Parse(Console.ReadLine());
                    var result = MultiphyString(inputString, times);
                    Console.WriteLine($"Result for task 1:{result}");
                    break;
                case "b":
                    Console.WriteLine("Input number 1:");
                    int number1 = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Input number 2:");
                    int number2 = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("\n -- Chose type swap---");
                    Console.WriteLine("\n1 - Swap use temp.");
                    Console.WriteLine("\n2 - Swap not use temp.");
                    Console.Write("\n - Your option?");
                    int typeSwap = Int32.Parse(Console.ReadLine());
                    if (typeSwap == 1)
                    {
                        SwapNumberUseTmp(number1, number2);
                    }
                    if (typeSwap == 2)
                    {
                        SwapNumberNotUseTmp(number1, number2);
                    }
                    break;
                case "c":
                    Console.WriteLine("Input string need Nomalize:");
                    var nomalizeString = Console.ReadLine();
                    Console.WriteLine($"String after nomalize:\nNumber1:{NomalizeString(nomalizeString)}");
                    break;
                case "d":
                    int size;
                    int[] arr;
                    //Input size of the array
                    Console.Write("Enter size of the array: ");
                    size = Convert.ToInt32(Console.ReadLine());
                    //Input array elements
                    Console.WriteLine("Enter {0} elements in array:", size);
                    arr = new int[size];
                    for (int i = 0; i < size; i++)
                    {
                        Console.Write("Number({0}) :", i + 1);
                        arr[i] = Convert.ToInt32(Console.ReadLine());
                    }
                    var countEvenAndOddNumbers = CountEvenAndOddNumbers(arr);
                    Console.WriteLine($"Result:{countEvenAndOddNumbers[0]} , {countEvenAndOddNumbers[1]}");
                    break;
                case "e":
                    Console.WriteLine($"Result:{Calculator()}");
                    break;
            }
            // Wait for the user to respond before closing.
            Console.Write("Press any key to close the console app...");
            Console.ReadKey();
        }
        public static string MultiphyString(string inputString, int times)
        {
            string result = "";
            for (int i = 0; i < times; i++)
            {
                result += inputString;
            }
            return result;
        }
        public static void SwapNumberUseTmp(int number1, int number2)
        {
            int temp = number1;
            number1 = number2;
            number2 = temp;
            Console.WriteLine($"Result of swapuse temp:\nNumber1:{number1}\nNumber2:{number2}");
        }
        public static void SwapNumberNotUseTmp(int number1, int number2)
        {
            (number1, number2) = (number2, number1);
            Console.WriteLine($"Result of swap not use temp:\nNumber1:{number1}\nNumber2:{number2}");
        }
        public static string NomalizeString(string inputString)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            var textAfterNomalizeString = textInfo.ToTitleCase(inputString);
            return textAfterNomalizeString;
        }
        public static int[] CountEvenAndOddNumbers(int[] arr)
        {
            int even = 0;
            int odd = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 0)
                    continue;
                if (arr[i] % 2 == 0)
                    even++;
                else
                    odd++;
            }
            var result = new List<int>() { even, odd };
            return result.ToArray();
        }
        public static int Calculator()
        {
            Console.WriteLine("--Calculator--");
            Console.WriteLine("Input number 1:");
            var number1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Input number 2:");
            var number2 = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the action to be performed?");
            Console.WriteLine("Press 1 for Addition");
            Console.WriteLine("Press 2 for Subtraction");
            Console.WriteLine("Press 3 for Multiplication");
            Console.WriteLine("Press 4 for Division \n");
            int action = Convert.ToInt32(Console.ReadLine());
            int result = 0;
            if (action == 1)
                result = number1 + number2;
            if(action==2)
                result = number1 - number2;
            if (action == 3)
                result = number2 * number2;
            if (action == 4)
                result = number1 / number2;

            return result;
        }
    }
}
